import {getData} from '~/utils/store-utils';
import Vue from 'vue';

export const state = () => ({
  customerTypes: []
})

export const mutations = {
  SET(state, customerTypes) {
    state.customerTypes = customerTypes
  },
  ADD(state, customerType) {
    state.customerTypes = state.customerTypes.concat(customerType);
  },
  DELETE(state, customerTypeId) {
    state.customerTypes = state.customerTypes.filter(i => i.id !== customerTypeId);
  },
  EDIT(state, newCustomerType) {
    let iIndex = state.customerTypes.findIndex(i => i.id === newCustomerType.id);
    Vue.set(state.customerTypes, iIndex, newCustomerType);
  }
}

export const actions = {
  async loadAllCustomerTypes({commit, dispatch}, params) {
    let customerTypes = await this.$axios.get('http://localhost:3000/api/customer-type', params);
    commit('SET', customerTypes.data.data);
  },
  async create({commit}, customerType) {
    let response = await this.$axios.post('api/customer-type', customerType);
    let savedIndustry = response.data.data;
    commit('ADD', savedIndustry);
    return savedIndustry;
  },
  async delete({commit}, customerType) {
    let response = await this.$axios.delete(`api/customer-type/${customerType.id}`);
    if(response.status === 200 || response.status === 204){
      commit('DELETE', customerType.id);
    }
  },
  async edit({commit}, customerType) {
    let response = await this.$axios.put(`api/customer-type/${customerType.id}`, customerType);
    let newCustomerType = response.data.data;
    commit('EDIT', newCustomerType);
    return newCustomerType;
  },
}

export const getters = {
  get: state => id => {
    return state.customerTypes.find(i => i.id === id) || {}
  }
}
