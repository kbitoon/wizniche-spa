import {getData} from '~/utils/store-utils';
import Vue from 'vue';

export const state = () => ({
  employees: []
})

export const mutations = {
  SET(state, employees) {
    state.employees = employees
  },
  ADD(state, employee) {
    state.employees = state.employees.concat(employee);
  },
  DELETE(state, employeeId) {
    state.employees = state.employees.filter(i => i.id !== employeeId);
  },
  EDIT(state, newEmployee) {
    let iIndex = state.employees.findIndex(i => i.id === newEmployee.id);
    Vue.set(state.employees, iIndex, newEmployee);
  }
}

export const actions = {
  async loadAllEmployees({commit, dispatch}, params) {
    let employees = await this.$axios.get('http://localhost:3000/api/employee', params);
    commit('SET', employees.data.data);
  },
  async create({commit}, employee) {
    let response = await this.$axios.post('api/employee', employee);
    let saveEmployee = response.data.data;
    commit('ADD', saveEmployee);
    return saveEmployee;
  },
  async delete({commit}, employee) {
    let response = await this.$axios.delete(`api/employee/${employee.id}`);
    if(response.status === 200 || response.status === 204){
      commit('DELETE', employee.id);
    }
  },
  async edit({commit}, employee) {
    let response = await this.$axios.put(`api/employee/${employee.id}`, employee);
    let newEmployee = response.data.data;
    commit('EDIT', newEmployee);
    return newEmployee;
  },
}

export const getters = {
  get: state => id => {
    return state.employees.find(i => i.id === id) || {}
  }
}
