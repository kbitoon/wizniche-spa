export const state = () => ({
  company: {}
});

export const mutations = {
  SET(state, company) {
    state.company = company
  }
};

export const actions = {
  async loadCompany({commit, dispatch}, params) {
    console.log(this.$auth.user.account_id);
    let response = await this.$axios.get(`http://localhost:3000/api/account/${this.$auth.user.account_id}`, params);
    commit('SET', response.data.data);
  },
  async edit({commit}, company) {
    let response = await this.$axios.put(`api/account/${company.id}`, company);
    let newCompany = response.data.data;
    commit('SET', newCompany);
    return newCompany;
  },
};

export const getters = {};
