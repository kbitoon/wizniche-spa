import {getData} from '~/utils/store-utils';
import Vue from 'vue';

export const state = () => ({
  industries: []
})

export const mutations = {
  SET(state, industries) {
    state.industries = industries
  },
  ADD(state, industry) {
    state.industries = state.industries.concat(industry);
  },
  DELETE(state, industryId) {
    state.industries = state.industries.filter(i => i.id !== industryId);
  },
  EDIT(state, newIndustry) {
    let iIndex = state.industries.findIndex(i => i.id === newIndustry.id);
    Vue.set(state.industries, iIndex, newIndustry);
  }
}

export const actions = {
  async loadAllIndustries({commit, dispatch}, params) {
    let industries = await this.$axios.get('http://localhost:3000/api/industry', params);
    commit('SET', industries.data.data);
  },
  async create({commit}, industry) {
    let response = await this.$axios.post('api/industry', industry);
    let savedIndustry = response.data.data;
    commit('ADD', savedIndustry);
    return savedIndustry;
  },
  async delete({commit}, industry) {
    let response = await this.$axios.delete(`api/industry/${industry.id}`);
    if(response.status === 200 || response.status === 204){
      commit('DELETE', industry.id);
    }
  },
  async edit({commit}, industry) {
    let response = await this.$axios.put(`api/industry/${industry.id}`, industry);
    let newIndustry = response.data.data;
    commit('EDIT', newIndustry);
    return newIndustry;
  },
}

export const getters = {
  get: state => id => {
    return state.industries.find(i => i.id === id) || {}
  }
}
