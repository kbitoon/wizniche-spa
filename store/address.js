import {getData} from '~/utils/store-utils';
import Vue from 'vue';

export const state = () => ({
  addresses: []
})

export const mutations = {
  SET(state, addresses) {
    state.addresses = addresses
  },
  ADD(state, address) {
    state.addresses = state.addresses.concat(address);
  },
  DELETE(state, addressId) {
    state.addresses = state.addresses.filter(i => i.id !== addressId);
  },
  EDIT(state, newCategory) {
    let iIndex = state.addresses.findIndex(i => i.id === newCategory.id);
    Vue.set(state.addresses, iIndex, newCategory);
  }
}

export const actions = {
  async loadAllAddress({commit, dispatch}, params) {
    let addresses = await this.$axios.get('http://localhost:3000/api/address', params);
    commit('SET', addresses.data.data);
  },
  async create({commit}, address) {
    let response = await this.$axios.post('api/address', address);
    let saveAddress = response.data.data;
    commit('ADD', saveAddress);
    return saveAddress;
  },
  async delete({commit}, address) {
    let response = await this.$axios.delete(`api/address/${address.id}`);
    if(response.status === 200 || response.status === 204){
      commit('DELETE', address.id);
    }
  },
  async edit({commit}, address) {
    let response = await this.$axios.put(`api/address/${address.id}`, address);
    let newAddress = response.data.data;
    commit('EDIT', newAddress);
    return newAddress;
  },
}

export const getters = {
  get: state => id => {
    return state.addresses.find(i => i.id === id) || {}
  }
}
