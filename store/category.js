import {getData} from '~/utils/store-utils';
import Vue from 'vue';

export const state = () => ({
  categories: []
})

export const mutations = {
  SET(state, categories) {
    state.categories = categories
  },
  ADD(state, category) {
    state.categories = state.categories.concat(category);
  },
  DELETE(state, categoryId) {
    state.categories = state.categories.filter(i => i.id !== categoryId);
  },
  EDIT(state, newCategory) {
    let iIndex = state.categories.findIndex(i => i.id === newCategory.id);
    Vue.set(state.categories, iIndex, newCategory);
  }
}

export const actions = {
  async loadAllCategories({commit, dispatch}, params) {
    let categories = await this.$axios.get('http://localhost:3000/api/category', params);
    commit('SET', categories.data.data);
  },
  async create({commit}, category) {
    let response = await this.$axios.post('api/category', category);
    let saveCategory = response.data.data;
    commit('ADD', saveCategory);
    return saveCategory;
  },
  async delete({commit}, category) {
    let response = await this.$axios.delete(`api/category/${category.id}`);
    if(response.status === 200 || response.status === 204){
      commit('DELETE', category.id);
    }
  },
  async edit({commit}, category) {
    let response = await this.$axios.put(`api/category/${category.id}`, category);
    let newCategory = response.data.data;
    commit('EDIT', newCategory);
    return newCategory;
  },
}

export const getters = {
  get: state => id => {
    return state.categories.find(i => i.id === id) || {}
  }
}
