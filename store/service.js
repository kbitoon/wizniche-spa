import {getData} from '~/utils/store-utils';
import Vue from 'vue';

export const state = () => ({
  services: []
})

export const mutations = {
  SET(state, services) {
    state.services = services
  },
  ADD(state, service) {
    state.services = state.services.concat(service);
  },
  DELETE(state, serviceId) {
    state.services = state.services.filter(i => i.id !== serviceId);
  },
  EDIT(state, newService) {
    let iIndex = state.services.findIndex(i => i.id === newService.id);
    Vue.set(state.services, iIndex, newService);
  }
}

export const actions = {
  async loadAllServices({commit, dispatch}, params) {
    let services = await this.$axios.get('http://localhost:3000/api/service', params);
    commit('SET', services.data.data);
  },
  async create({commit}, service) {
    let response = await this.$axios.post('api/service', service);
    let saveService = response.data.data;
    commit('ADD', saveService);
    return saveService;
  },
  async delete({commit}, service) {
    let response = await this.$axios.delete(`api/service/${service.id}`);
    if(response.status === 200 || response.status === 204){
      commit('DELETE', service.id);
    }
  },
  async edit({commit}, service) {
    let response = await this.$axios.put(`api/service/${service.id}`, service);
    let newService = response.data.data;
    commit('EDIT', newService);
    return newService;
  },
}

export const getters = {
  get: state => id => {
    return state.services.find(i => i.id === id) || {}
  }
}
