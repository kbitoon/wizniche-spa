export default function(ctx) {
  let user = ctx.$auth.user;
  if (user) {
    if (user.scope.includes('administrator')) {
      ctx.layout = 'administrator'
    } else {
      ctx.layout = 'subscriber'
    }
  }
}
